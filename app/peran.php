<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class peran extends Model
{
    public function film()
    {
        return $this->belongsToMany('App\film' , 'perans' , 'film_id');
    }
    public function Cast()
    {
        return $this->belongsToMany('App\Cast' , 'perans' , 'cast_id');
    }
}
