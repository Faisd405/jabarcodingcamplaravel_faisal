@extends('layout.master')
@section('judul')
    <h4>Show Cast</h4>
@endsection
@section('content')

    <h2>Show Cast {{$cast->id}}</h2>
    <br>
    <h4>{{$cast->nama}}</h4>
    <p>{{$cast->umur}}</p>
    <p>{{$cast->bio}}</p>
    <p>{{$cast->tes}}</p>
@endsection
