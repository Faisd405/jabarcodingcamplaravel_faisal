@extends('layout.master')
@section('judul')
    <h4>Halaman Genre (Eloquent One to Many)</h4>
@endsection
@section('content')

<h3>One to Many</h3>
<table>
  <thead>
   <tr>
    <th>Genre</th>
    <th>Film</th>
   </tr>
  <tbody>
   @foreach($genre as $value)
   <tr>
    <td>{{$value->nama}}</td>
    <td>
       @foreach($value->film as $b)
       {{$b->judul}},
       @endforeach
    </td>
   </tr>
   @endforeach
  <tbody>
  </thead>
</table>

@endsection
