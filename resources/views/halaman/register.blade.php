<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>

<body>
    <form action="welcome">
        @csrf
        <h1>Buat Account Baru</h1>
        <h2>Sign Up Form</h2>

        <p>First Name:</p>
        <input type="text" id="first_name" name="first_name">

        <p>Last Name:</p>
        <input type="text" id="last_name" name="last_name">

        <p>Gender:</p>
        <input type="radio" id="Male" name="Gender" value="Male">
        <label for="Male">Male</label><br>
        <input type="radio" id="Female" name="Gender" value="Female">
        <label for="Female">Female</label><br>
        <input type="radio" id="Other" name="Gender" value="Other">
        <label for="Other">Other</label>

        <p>Nasionality:</p>
        <select id="Nasionality" name="Nasionality">
            <option value="Indonesia">Indonesia</option>
            <option value="English">English</option>
            <option value="Other">Other</option>
        </select>

        <p>Language Select:</p>
        <input type="checkbox" id="language1" name="language" value="Bahasa Indonesia">
        <label for="language1"> Bahasa Indonesia</label><br>
        <input type="checkbox" id="language2" name="language" value="English">
        <label for="language2"> English</label><br>
        <input type="checkbox" id="language3" name="language" value="Other">
        <label for="language3"> Other</label>
        
        <p>Bio: </p>
        <textarea name="Bio" id="Bio" cols="30" rows="10"></textarea>
        <br>
        <button type="submit">Sign Up</button>
    </form>




</body>

</html>