@extends('layout.master')
@section('judul')
    <h4>Halaman Profile (Eloquent One to One)</h4>
@endsection
@section('content')

Nama : {{Auth::user()->name}}
<br>
Email : {{Auth::user()->email}}
<br>

@if (!Auth::user()->profile == NULL)

Umur : {{Auth::user()->profile->umur}}
<br>
Bio : {{Auth::user()->profile->bio}}
<br>
Alamat : {{Auth::user()->profile->alamat}}
<a href="#">Edit Profile</a>
@else
    <a href="#">Bikin Profile</a>
@endif


@if (!Auth::user()->profile == NULL)
<hr>
<h3>One to one</h3>
<table>
  <thead>
   <tr>
    <th>nama</th>
    <th>email</th>
    <th>bio</th>
   </tr>
  <tbody>
   @foreach($user as $value)
   <tr>
    <td>{{$value->name}}</td>
    <td>{{$value->email}}</td>
    <td>{{$value->profile->umur}}</td>
   </tr>
   @endforeach
  <tbody>
  </thead>
</table>
@endif

@endsection
