<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Tugas Awal
Route::get('/home','HomeController@home');
Route::get('/register','AuthController@form');
Route::get('/welcome','AuthController@kirim');

Route::get('/', function (){
    return view('halaman.home');
});

// Table
Route::get('/table', function (){
    return view('halaman.table');
});
Route::get('/data-tables', function (){
    return view('halaman.data-tables');
});

//Cast
Route::resource('cast', 'CastController')->middleware('auth');

//Profiles (ELOQUENT ONE TO ONE)
Route::resource('profile', 'ProfileController')->only(
    'index' , 'update', 'tambah'
);

//Genre (ELOQUENT ONE TO Many)
Route::resource('genre', 'GenreController')->only(
    'index'
);

//Auth
Auth::routes();

//Logins
Route::get('/home', 'HomeController@index')->name('home');
